﻿string input = "hello world!!";
string result = input.DaoChuoi();
Console.WriteLine(result);
public static class Text
{
    public static string DaoChuoi(this string input)
    {
        if (string.IsNullOrEmpty(input))
        {
            return input;
        }
        char[] chars = input.ToCharArray();
        return string.Join("", chars.Reverse());
    }
}